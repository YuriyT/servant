const _ = require('lodash');
const nconf = require('nconf');
const assert = require('assert-plus');
const rda = require('require-dir-all');
const express = require('express');
const swaggerTools  = require('swagger-tools');
const swaggerUi = require('swagger-ui-express');

const ClassySwaggerRouter = require('classy-swagger-router');

module.exports = class Servant {
  constructor(...args) {
    const logger = args.pop();
    if (logger) {
      assert.object(logger, 'logger');
      this.logger = logger;
    }

    const config = args.pop();
    if (config) {
      assert.object(config, 'config');
      this.config = config;
    }

    this.app = express();
    this.httpServer = null;
    this.swaggerMiddleware = [];
    this.swaggerAuth = null;

    this.classySwaggerRouter = new ClassySwaggerRouter(
      ...args,
      this.config,
      this.logger);

    const port = parseInt(this.config.server.port, 10);
    this.port = isNaN(port) ? this.config.server.port : (port >= 0 ? port : false);
  }

  get server() {
    return this.app
  }

  set swaggerSecurity(swaggerAuth) {
    this.swaggerAuth = swaggerAuth;
  }

  static makeConfig(directory, options) {

    let rawConfig = rda(directory, {
      recursive: true
    });

    let api;

    if (options.api) {
      api = _.merge({}, ..._.map(rawConfig[options.api], (configItem) => configItem));
      delete rawConfig[options.api]
    }

    let ret = _.merge({}, ..._.map(rawConfig, (configItem) => configItem));

    const match = new RegExp(`^${ret.envSuffix}_`, 'i');
    nconf.env({
      separator: '_',
      match,
      lowerCase: true,
    });

    let configFromEnv = nconf.get() && nconf.get()[ret.envSuffix.toLowerCase()] || {};

    if (options.api) {
      ret.api = api;
    }

    return Object.freeze(Object.assign(ret, configFromEnv))
  }


  use(middleware) {
    this.app.use(middleware)
  }

  staticFolder(folder) {
    this.app.use(express.static(folder));
  }

  useSwagger(middleware) {
    this.swaggerMiddleware.push(middleware)
  }

  start() {
    swaggerTools.initializeMiddleware(this.config.api, (middleware) => {
      // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
      this.app.use(middleware.swaggerMetadata());

      // Validate Swagger requests
      this.app.use(middleware.swaggerValidator());

      // Set Swagger security
      this.app.use(middleware.swaggerSecurity(this.swaggerAuth));

      // Route validated requests to appropriate controller
      this.app.use(this.classySwaggerRouter.router);

      for(const middleware of this.swaggerMiddleware) {
        this.app.use(middleware)
      }

      if (this.config.swaggerUI) {
        this.app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(this.config.api, true));
      }
    });
    this.httpServer = this.app.listen(this.port);
  }

  stop() {
    // this.logger.info('Shutting down the server');
    if (this.httpServer !== null ){
      this.httpServer.close();
      this.httpServer = null;
    }
    this.logger = null;
  }

};